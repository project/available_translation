<?php

namespace Drupal\available_translation\Service;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Builds the available translation links.
 */
class AvailableTranslationService {
  use StringTranslationTrait;

  /**
   * Controls the lifecycle of requests.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * Available languages.
   *
   * @var array|\Drupal\Core\Language\LanguageInterface[]
   */
  private $availableLanguages = [];

  /**
   * Current request language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  private $currentLanguage;

  /**
   * Node.
   *
   * @var \Drupal\node\Entity\Node
   */
  private $node;

  /**
   * Constructs an AvailableTranslationService object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Controls the lifecycle of requests.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager.
   */
  public function __construct(RequestStack $request_stack, LanguageManagerInterface $language_manager) {
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
    $this->node = $this->requestStack->getCurrentRequest()->get('node');
    if ($this->node instanceof Node) {
      $this->availableLanguages = $this->node->getTranslationLanguages();
    }
    $this->currentLanguage = $this->languageManager->getCurrentLanguage();
  }

  /**
   * Builds the available translation links glued with given delimiter.
   *
   * @param array $config
   *   Configuration array of the block plugin.
   *
   * @return string
   *   Available translation links concatenated with delimiter
   */
  private function getLinks(array $config) {
    $links = [];
    foreach ($this->availableLanguages as $language) {
      $options = ["language" => $language];
      if ($language->getId() === $this->currentLanguage->getId()) {
        $options["attributes"]["class"] = "active";
      }
      $url = Url::fromRoute("entity.node.canonical", ["node" => $this->node->id()], $options);
      $link_text = $config["is_langcode"] ? $language->getId() : $language->getName();
      $link_text = $config["is_uppercase"] ? strtoupper($link_text) : $link_text;
      $link = Link::fromTextAndUrl($link_text, $url);
      $links[] = $link->toString();
    }
    $links = implode($config["delimiter"], $links);
    $header = $config["is_header"] ? $this->t("Available in:") . " " : "";
    $links = $links ? $header . $links : $this->t("No translation available!");

    return $links;
  }

  /**
   * Builds the available translation links through theme_links template.
   *
   * @param array $config
   *   Configuration array of the block plugin.
   *
   * @return array
   *   Array of links
   */
  private function getThemeLinks(array $config) {
    $links = [];
    foreach ($this->availableLanguages as $language) {
      $title = $config["is_langcode"] ? $language->getId() : $language->getName();
      $title = $config["is_uppercase"] ? strtoupper($title) : $title;
      $links[] = [
        "title" => $title,
        "url" => Url::fromRoute(
          "entity.node.canonical", ["node" => $this->node->id()], ["language" => $language]
        ),
        "attributes" => ["class" => ["links__link"]],
      ];
    }

    return $links;
  }

  /**
   * Render array of the links block.
   *
   * @param array $config
   *   Configuration array of the block plugin.
   *
   * @return array
   *   Render array
   */
  public function getThemeLinksBuild(array $config) {
    $build["links"] = [
      "#theme" => "links",
      "#links" => $this->getThemeLinks($config),
      "#set_active_class" => TRUE,
      "#attributes" => [
        "class" => [
          "links__available-translation",
          "links__available-translation-" . $config["position"],
        ],
      ],
      "#cache" => [
        "max-age" => 0,
      ],
    ];

    if ($config["is_header"]) {
      $build["links"]["#heading"] = [
        "text" => $this->t("Available in:"),
        "level" => $config["header_tag"],
        "attributes" => [
          "class" => [
            "links__heading-available-translation",
            "links__heading-available-translation-" . $config["position"],
          ],
        ],
      ];
    }

    return $build;
  }

  /**
   * Render array of the available_translation_template block.
   *
   * @param array $config
   *   Configuration array of the block plugin.
   *
   * @return array
   *   Render array
   */
  public function getThemeAvailableTranslationTemplateBuild(array $config) {
    $build["available_translation_template"] = [
      "#theme" => "available_translation_template",
      "#text" => $this->getLinks($config),
      "#position" => $config["position"],
      "#attached" => [
        "library" => ["available_translation/available_translation"],
      ],
      "#cache" => [
        "max-age" => 0,
      ],
    ];

    return $build;
  }

}
