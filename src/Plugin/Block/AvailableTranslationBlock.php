<?php

namespace Drupal\available_translation\Plugin\Block;

use Drupal\available_translation\Service\AvailableTranslationService;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an Available Translation block.
 *
 * @Block(
 *   id = "available_translation",
 *   admin_label = @Translation("Available Translation"),
 *   category = @Translation("Menus")
 * )
 */
class AvailableTranslationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Service for building the available translation links.
   *
   * @var \Drupal\available_translation\Service\AvailableTranslationService
   */
  protected $service;

  /**
   * AvailableTranslationBlock constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\available_translation\Service\AvailableTranslationService $service
   *   Service for building the available translation links.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AvailableTranslationService $service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get("available_translation.service")
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      "template" => "links",
      "is_header" => FALSE,
      "header_tag" => "div",
      "is_langcode" => FALSE,
      "is_uppercase" => FALSE,
      "delimiter" => " | ",
      "position" => "left",
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
    $default_config = $this->defaultConfiguration();

    $form["template"] = [
      "#type" => "radios",
      "#title" => $this->t("Template"),
      "#options" => [
        "links" => $this->t("Links (theme_links)"),
        "available_translation_template" => $this->t("Available translation template"),
      ],
      "#default_value" => $config["template"] ?? $default_config["template"],
    ];

    $form["is_header"] = [
      "#type" => "checkbox",
      "#title" => $this->t("Display header") . " (" . $this->t("Available in:") . ")",
      "#description" => $this->t("You can translate the header to any desired language"),
      "#default_value" => $config["is_header"] ?? $default_config["is_header"],
    ];

    $options = [
      "div", "p", "span", "strong", "b", "small", "sub", "sup", "i", "em",
      "pre", "cite", "code", "h1", "h2", "h3", "h4", "h5", "h6",
    ];
    $form["header_tag"] = [
      "#type" => "select",
      "#title" => $this->t("Header tag"),
      "#options" => array_combine($options, $options),
      "#default_value" => $config["header_tag"] ?? $default_config["header_tag"],
      "#states" => [
        "visible" => [
          ":input[name='settings[template]']" => ["value" => "links"],
          ":input[name='settings[is_header]']" => ['checked' => TRUE],
        ],
      ],
    ];

    $form["is_langcode"] = [
      "#type" => "checkbox",
      "#title" => $this->t("Use language codes"),
      "#description" => $this->t("Ex. en instead of English"),
      "#default_value" => $config["is_langcode"] ?? $default_config["is_langcode"],
    ];

    $form["is_uppercase"] = [
      "#type" => "checkbox",
      "#title" => $this->t("Use upper case language names/codes"),
      "#default_value" => $config["is_uppercase"] ?? $default_config["is_uppercase"],
    ];

    $form["delimiter"] = [
      "#type" => "textfield",
      "#title" => $this->t("Delimiter"),
      "#description" => $this->t("Example: ' | ' for 'Armenian | English | French'"),
      "#default_value" => $config["delimiter"] ?? $default_config["delimiter"],
      "#states" => [
        "visible" => [
          ":input[name='settings[template]']" => ["value" => "available_translation_template"],
        ],
      ],
    ];

    $form["position"] = [
      "#type" => "radios",
      "#title" => $this->t("Position"),
      "#options" => [
        "left" => $this->t("Left"),
        "right" => $this->t("Right"),
      ],
      "#default_value" => $config["position"] ?? $default_config["position"],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->setConfiguration([
      "template" => $values["template"],
      "is_header" => $values["is_header"],
      "header_tag" => $values["header_tag"],
      "is_langcode" => $values["is_langcode"],
      "is_uppercase" => $values["is_uppercase"],
      "delimiter" => $values["delimiter"],
      "position" => $values["position"],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $build = [];
    if ($config["template"] === "links") {
      $build = $this->service->getThemeLinksBuild($config);
    }
    elseif ($config["template"] === "available_translation_template") {
      $build = $this->service->getThemeAvailableTranslationTemplateBuild($config);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    if (!$account->hasPermission("access available_translation links")) {
      return AccessResult::forbidden();
    }
    return parent::blockAccess($account);
  }

}
